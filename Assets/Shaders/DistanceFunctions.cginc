﻿// Sphere
// s: radius
float sdSphere(float3 p, float s)
{
	return length(p) - s;
}

// Box
// b: size of box in x/y/z
float sdBox(float3 p, float3 b)
{
	float3 d = abs(p) - b;
	return min(max(d.x, max(d.y, d.z)), 0.0) +
		length(max(d, 0.0));
}

float sdRoundBox(in float3 p, in float3 b, in float r)
{
	float3 q = abs(p) - b;
	return length(max(q, 0.0)) + min(max(q.x, max(q.y, q.z)), 0.0) - r;
}

float sdPlane(float3 p, float4 n)
{
	return dot(p, n.xyz) + n.w;
}

float sdTorus(float3 p, float2 t)
{
	float2 q = float2(length(p.xz) - t.x, p.y);
	return length(q) - t.y;
}

float sdCappedTorus( float3 p, float2 sc,  float ra,  float rb)
{
	p.x = abs(p.x);
	float k = (sc.y*p.x > sc.x*p.y) ? dot(p.xy, sc) : length(p.xy);
	return sqrt(dot(p, p) + ra * ra - 2.0*ra*k) - rb;
}

float PrBox2Df(float2 p, float2 b)
{
	float2 d;
	d = abs(p) - b;
	return min(max(d.x, d.y), 0.) + length(max(d, 0.));
}

float PrRoundCylDf(float3 p, float r, float rt, float h)
{
	float dxy, dz;
	dxy = length(p.xy) - r;
	dz = abs(p.z) - h;
	return min(min(max(dxy + rt, dz), max(dxy, dz + rt)), length(float2(dxy, dz) + rt) - rt);
}

float sdCappedCylinder(float3 p, float3 a, float3 b, float r) 
{
	float3  ba = b - a;
	float3  pa = p - a;
	float baba = dot(ba, ba);
	float paba = dot(pa, ba);
	float x = length(pa*baba - ba * paba) - r * baba;
	float y = abs(paba - baba * 0.5) - baba * 0.5;
	float x2 = x * x;
	float y2 = y * y*baba;
	float d = (max(x, y) < 0.0) ? -min(x2, y2) : (((x > 0.0) ? x2 : 0.0) + ((y > 0.0) ? y2 : 0.0));
	return sign(d)*sqrt(abs(d)) / baba;
}

// BOOLEAN OPERATORS //

Object opUnion(Object o1, Object o2) {
	// min(o1, o2)
	if (o1.distance < o2.distance) return o1;
	return o2;
}

Object opUnionSmooth(Object o1, Object o2, float k) {
	Object result = o1;

	float h = clamp(0.5 + 0.5*(o2.distance - o1.distance) / k, 0.0, 1.0);
	result.material.albedo = lerp(o2.material.albedo, o1.material.albedo, h);
	result.material.specular = lerp(o2.material.specular, o1.material.specular, h);
	result.material.reflectivity = lerp(o2.material.reflectivity, o1.material.reflectivity, h);

	result.distance = lerp(o2.distance, o1.distance, h) - k * h*(1.0 - h);
	h = clamp(0.5 + 0.5*(o2.distance - o1.distance) / 0, 0, 1.0);
	result.id = round(lerp(float(o2.id), float(o1.id), h));
	return result;
}


// Union
float opU(float d1, float d2)
{
	return min(d1, d2);
}

float4 opU(float4 d1, float4 d2)
{
	return (d1.w < d2.w) ? d1 : d2;
}

// Subtraction
float opS(float d1, float d2)
{
	return max(-d1, d2);
}

// Intersection
float opI(float d1, float d2)
{
	return max(d1, d2);
}

// Mod Position Axis
float pMod1 (inout float p, float size)
{
	float halfsize = size * 0.5;
	float c = floor((p+halfsize)/size);
	p = fmod(p+halfsize,size)-halfsize;
	p = fmod(-p+halfsize,size)-halfsize;
	return c;
}



float3 opRep(float3 p, float3 c)
{
	return fmod(p + 0.5*c, c) - 0.5*c;
}

float4 opUS(float4 d1, float4 d2, float k)
{
	float h = clamp(0.5 + 0.5*(d2.w - d1.w) / k, 0.0, 1.0);
	float3 color = lerp(d2.rgb, d1.rgb, h);
	float dist = lerp(d2.w, d1.w, h) - k * h*(1.0 - h);
	return float4(color, dist);
}

float opSS(float d1, float d2, float k)
{
	float h = clamp(0.5 - 0.5*(d2 + d1) / k, 0.0, 1.0);
	return lerp(d2, -d1, h) + k * h*(1.0 - h);
}

Object opSS(Object d1, Object d2, float k)
{
	Object o = d1;
	float h = clamp(0.5 - 0.5*(d2.distance + d1.distance) / k, 0.0, 1.0);
	o.distance = lerp(d2.distance, -d1.distance, h) + k * h*(1.0 - h);

	return o;
}

float opIS(float d1, float d2, float k)
{
	float h = clamp(0.5 - 0.5*(d2 - d1) / k, 0.0, 1.0);
	return lerp(d2, d1, h) + k * h*(1.0 - h);
}

float3 rotateY(float3 v, float degree) {
	float rad = 0.017453925 * degree;
	float cosY = cos(rad);
	float sinY = sin(rad);
	return float3(cosY * v.x - sinY * v.z, v.y, sinY * v.x + cosY * v.z);
}

float4 opElongate(in float3 p, in float3 h)
{
	//return vec4( p-clamp(p,-h,h), 0.0 ); // faster, but produces zero in the interior elongated box

	float3 q = abs(p) - h;
	return float4(max(q, 0.0), min(max(q.x, max(q.y, q.z)), 0.0));
}



float3 opTwistY(float3 p, float k)
{
	float c = cos(k*p.y);
	float s = sin(k*p.y);
	float2x2  m = float2x2(c, -s, s, c);
	float3  q = float3(mul(p.xz,m), p.y); 
	return q;
}

float3 opTwistX(float3 p, float k)
{

	float c = cos(k*p.x);
	float s = sin(k*p.x);
	float2x2  m = float2x2(c, -s, s, c);
	float3  q = float3(mul(p.yz, m), p.x);
	return q;
}

float3 opCheapBend(float3 p, float k)
{
	float c = cos(k*p.x);
	float s = sin(k*p.x);
	float2x2  m = float2x2(c, -s, s, c);
	float3 q = float3(mul(m, p.xy), p.z);
	return q;
}


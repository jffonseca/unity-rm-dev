

// 0 = lattice
// 1 = simplex
// 2 = bounded value noise
// 3 = Shane's 4-tap noise (ultra-fast!)
// 4 = Shane's 4-tap smooth noise (still faster and continuous)
#define NOISE 4 

float hash(float3 p)  // replace this by something better
{
    p  = 17.0*frac( p*0.3183099+float3(.11,.17,.13) );
    return frac( p.x*p.y*p.z*(p.x+p.y+p.z) );
}



// simple extension for smin to calculate 2 smin's at the same time [tom]s
float2 smin2(float2 a, float2 b, float k) {
   float2 h = max(k-abs(a-b),0.0);
   return min(a, b) - h*h*0.25/k;
}

// http://iquilezles.org/www/articles/smin/smin.htm
float smax( float a, float b, float k )
{
    float h = max(k-abs(a-b),0.0);
    return max(a, b) + h*h*0.25/k;
}

// http://iquilezles.org/www/articles/boxfunctions/boxfunctions.htm
float2 iBox( in float3 ro, in float3 rd, in float3 rad ) 
{
    float3 m = 1.0/rd;
    float3 n = m*ro;
    float3 k = abs(m)*rad;
    float3 t1 = -n - k;
    float3 t2 = -n + k;
	float tN = max( max( t1.x, t1.y ), t1.z );
	float tF = min( min( t2.x, t2.y ), t2.z );
	if( tN > tF || tF < 0.0) return float2(-1.0, -1.0);
	return float2( tN, tF );
}

//---------------------------------------------------------------
//
// A smooth but random SDF. For each cell, it places a sphere of
// random size in each corner and computer the smooth minimum.
//
//---------------------------------------------------------------

float noiseSDF( in float3 p )
{
#if NOISE==0
    float3 i = floor(p);
    float3 f = frac(p);

    const float G1 = 0.30;
    const float G2 = 0.75;
    
	#define RAD(r) ((r)*(r)*G2)
    #define SPH(i,f,c) length(f-c)-RAD(hash(i+c))
    
    return smin(smin(smin(SPH(i,f,float3(0,0,0)),
                          SPH(i,f,float3(0,0,1)),G1),
                     smin(SPH(i,f,float3(0,1,0)),
                          SPH(i,f,float3(0,1,1)),G1),G1),
                smin(smin(SPH(i,f,float3(1,0,0)),
                          SPH(i,f,float3(1,0,1)),G1),
                     smin(SPH(i,f,float3(1,1,0)),
                          SPH(i,f,float3(1,1,1)),G1),G1),G1);
#elif NOISE == 1
    const float K1 = 0.333333333;
    const float K2 = 0.166666667;
    
    float3 i = floor(p + (p.x + p.y + p.z) * K1);
    float3 d0 = p - (i - (i.x + i.y + i.z) * K2);
    
    float3 e = step(d0.yzx, d0);
	float3 i1 = e*(1.0-e.zxy);
	float3 i2 = 1.0-e.zxy*(1.0-e);
    
    float3 d1 = d0 - (i1  - 1.0*K2);
    float3 d2 = d0 - (i2  - 2.0*K2);
    float3 d3 = d0 - (1.0 - 3.0*K2);
    
    float r0 = hash( i+0.0 );
    float r1 = hash( i+i1 );
    float r2 = hash( i+i2 );
    float r3 = hash( i+1.0 );

    const float G1 = 0.20;
    const float G2 = 0.50;

    #define SPH(d,r) length(d)-r*r*G2

    return smin( smin(SPH(d0,r0),
                      SPH(d1,r1),G1),
                 smin(SPH(d2,r2),
                      SPH(d3,r3),G1),G1);
#elif NOISE == 2
    // Value noise biased and roughly bounded to |gradient| <= 1 everywhere.
    p *= 1.5; // scale to get similar density
    float3 i = floor(p);
    float3 f = frac(p);
    f = f*f*(3.0-2.0*f);
	
    return (lerp(lerp(lerp( hash(i+float3(0,0,0)),
                        hash(i+float3(1,0,0)),f.x),
		lerp( hash(i+float3(0,1,0)),
                        hash(i+float3(1,1,0)),f.x),f.y),
		lerp(lerp( hash(i+float3(0,0,1)),
                        hash(i+float3(1,0,1)),f.x),
			lerp( hash(i+float3(0,1,1)),
                        hash(i+float3(1,1,1)),f.x),f.y),f.z)-.3)*.7;
#else
    const float scale = 1.5; // scale the domain, because it's more density of spheres
    p *= 1.0/scale;

    // Shane's fast 4-tap noise.
    float4 d; float3 dp;
    dp = frac(p  - float3(.81, .62, .53)) - .5; d.x = length(dp);
    p.xy = float2(p.y-p.x, p.y + p.x)*.7071 ;
    dp = frac(p - float3(.39, .2, .11)) - .5; d.y = length(dp);
    p.yz = float2(p.z-p.y, p.z + p.y)*.7071;
    dp = frac(p - float3(.62, .24, .06)) - .5; d.z = length(dp);
    p.xz = float2(p.z-p.x, p.z + p.x)*.7071;
    dp = frac(p - float3(.2, .82, .64)) - .5; d.w = length(dp);

  #if NOISE == 3
    // Obtain the minimum, and you're done.
    d.xy = min(d.xz, d.yw);
    float dd = min(d.x, d.y);
  #else
    // Obtain the smooth minimum this time.
    const float r = .1; // smoothing radius
    d.xy = smin2(d.xz, d.yw, r);
    float dd = smin(d.x, d.y, r);
  #endif

    return (dd - .25)*scale;
#endif
}



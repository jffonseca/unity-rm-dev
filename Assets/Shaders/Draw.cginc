﻿



Object createObject(int id, float3 p)
{
	Object o;
	o.distance = 0;
	o.id = id;
	if (_ObjectType[id].x == 0)
	{
		o.distance = sdBox((mul(_ObjectTransformMatrix[id], float4(p, 1)).xyz), float3(1, 1, 1));
	}
	else if(_ObjectType[id].x == 1)
	{
		o.distance = sdSphere((mul(_ObjectTransformMatrix[id], float4(p, 1)).xyz), float2(1, 1));
	}
	else if (_ObjectType[id].x == 2)
	{
        o.distance = sdSphere((mul(_ObjectTransformMatrix[id], float4(p, 1)).xyz), float2(1, 1));
	}
	else if(_ObjectType[id].x == 3)
	{
		o.distance = sdTorus((mul(_ObjectTransformMatrix[id], float4(p, 1)).xyz), float2(1.0, 0.5));
	}
		
	o.material.albedo = _MaterialAlbedo[id];
	o.material.specular = _MaterialParameters[id].x;
	o.material.reflectivity = _MaterialParameters[id].y;
	return o;
}

Object distanceFieldEditor(float3 p)
{
	Object scene = createObject(0, p);
	float3 pp = p;
    [fastopt]
	for (int i = 1; i < _NumberOfObjects; i++)
	{	
		Object sphere = createObject(i, pp);
		scene = opUnionSmooth(scene, sphere,0.8);
	}
	return scene;
}


// rotation matrix
const float3x3 m = float3x3(0.00, 0.80, 0.60, -0.80, 0.36, -0.48, -0.60, -0.48, 0.64);

Object distanceFieldCustom( float3 p)
{
	
	Object scene = createObject(0, p);

	 // fbm
    float t = 0.0;
	float s = 1.0;
	
	float3 pp = (mul(_ObjectTransformMatrix[0], float4(p, 1)).xyz);
    [fastopt]
    for( int i=0; i<10; i++ )
    {
		float3 ppp = pp ;
		//ppp.y *= sin(ppp.x+_Time);
    	//scene.distance = smax( scene.distance, -noiseSDF(ppp*10)*s, 0.2*s  );
        t += scene.distance;
        //pp = 0.41*mul(m,pp); // next octave
		pp *= 0.45;
        s = 0.950*s;
    }
    t = 1.0+t*2.0; t = t*t;


	return scene;
}


Object distanceFieldCustom2(float3 p)
{
	Object obj = createObject(0, p);
	return obj;//opUnionSmooth(obj, floor, 0);
} 
// fresnel https://www.shadertoy.com/view/4tyXDR
// https://www.shadertoy.com/view/WlXXzS https://www.shadertoy.com/view/XlK3D3 https://www.shadertoy.com/view/4ds3RN


Object distanceFieldCustom3(float3 p)
{

	Object flr = createObject(0, p);
    //Object obj = createObject(1, p);
	
	if ( distance(p.xz, float2(0, 0)) <  10 ) {
		//flr.distance += fbm4D(p*0.10) * 1* (1-(distance(p.xz, float2( 0, 0))/10));
	}

	return flr;//opUnionSmooth(flr, obj, 0.5);
} 
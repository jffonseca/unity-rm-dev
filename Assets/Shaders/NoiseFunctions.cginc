﻿#include "noiseSimplex.cginc"
uniform sampler2D _NoiseTexture;

float noise( float3 x)
{
	float3 p = floor(x);
	float3 f = frac(x);
	f = f * f*(3.0 - 2.0*f);
	float2 uv = (p.xy + float2(37.0, 239.0)*p.z) + f.xy;
	float2 rg = tex2Dlod(_NoiseTexture, float4((uv + 0.5) / 256,0,0)).yx;//textureLod(_noiseTexture, (uv + 0.5) / 256.0, 0.0).yx;
 
	return -1.0 + 2.0*lerp(rg.x, rg.y, f.z);
}

  
float fbm4D(float3 s)
{
	float amplitude = 0.5;
	int octaves = 2;
	float value = 0.0;
	float amp = amplitude;
	float dispScale = 4.5;
	float3 uv = s.xyz;

	// Loop of octaves
	for (int i = 0; i < octaves; i++) {
		value += amp * snoise(float4(uv,_Time.x*5)) * dispScale;//snoise4D(float4(uv*noiseScale,time*timeScale,time*timeScale));
		uv *= 0.9;
		amp *= 0.2;
	
	}
	float3 f = frac(s);

	return value;
}

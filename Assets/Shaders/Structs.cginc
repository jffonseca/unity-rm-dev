//- RAY

struct Ray
{
	float3 origin;
	float3 direction;
	float3 energy;
};


struct Material
{
	float3 albedo;
	float specular;
	float reflectivity;
};

struct Object
{
	float3 hitPosition;
	float distance;

	int id;
	float3 position;
	float3 normal;
	float3 size;

	Material material;
};


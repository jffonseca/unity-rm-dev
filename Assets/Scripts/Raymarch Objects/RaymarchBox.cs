﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RaymarchBox : RaymarchObject
{
    private const string primitive = "BOX";
    [Header(primitive)]
    [Range(0.0f,2.0f)]
    public float _Roundness; 
    private float oldRoundness;


    string mSdf = "float4(_MaterialAlbedo[__index].rgb, sdRoundBox(mul(_ObjectTransformMatrix[__index], float4(p, 1)).xyz, float3(1,1,1), __Roundness))";

    private void Update()
    {
        setStateChanged(false);
        if (_Roundness != oldRoundness) 
        {
            oldRoundness = _Roundness;
            setStateChanged(true);
        }
    }
    public override string GetDistanceFunction()
    {
        return string.Format(
            mSdf.Replace("__Roundness", _Roundness+"")
        );
    }
}

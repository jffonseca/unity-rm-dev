﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RaymarchObject : MonoBehaviour
{

    public enum Primitive { Box, Sphere, Mobius, Torus, Slime };
    [Header("Primitive")]
    public Primitive _Primitive;

    [Header("Material")]
    public Color _Albedo;
    public Texture2D _AlbedoTexture;
    public Texture2D _NormalTexture;
    public float _Specular;
    [Range(0.0f, 1.0f)]
    public float _Reflectivity;


    private int mSelectedPrimitive;
    private bool mStateChanged;
    private int mObjectType;

    private static string[] mSdfList = new string[4]
    {
     "float4(_MaterialAlbedo[__index].rgb, sdBox(mul(_ObjectTransformMatrix[__index], float4(p, 1)).xyz, float3(1,1,1)))",
     "float4(_MaterialAlbedo[__index].rgb, sdRoundBox(mul(_ObjectTransformMatrix[__index], float4(p, 1)).xyz, float3(1,1,1), 0.5))",
     "float4(_MaterialAlbedo[__index].rgb, sdPlane(mul(_ObjectTransformMatrix[__index], float4(p, 1)).xyz, float4(0,1,0,0)))",
     "float4(_MaterialAlbedo[__index].rgb, sdTorus(mul(_ObjectTransformMatrix[__index], float4(p, 1)).xyz, float2(1.0,0.5)))"
    };

    void Update()
    {
        mStateChanged = false;
        if (mSelectedPrimitive != (int)_Primitive)
        {
            mSelectedPrimitive = (int)_Primitive;
            mStateChanged = true;
        }
    }

    public void Start()
    {
      
    
    }

    public Color GetAlbedo()
    {
        return _Albedo;
    }

    public Texture2D GetAlbedoTexture()
    {
        return _AlbedoTexture;
    }

    public Texture2D GetNormalTexture()
    {
        return _NormalTexture;
    }

    public float GetSpecular()
    {
        return _Specular;
    }

    public float GetReflectivity()
    {
        return _Reflectivity;
    }

    public int GetObjectType()
    {
        return (int)_Primitive;
    }

    public Matrix4x4 GetTransform()
    {
        return gameObject.transform.worldToLocalMatrix;
    }

    public virtual string GetDistanceFunction()
    {
        return string.Format(
            mSdfList[mSelectedPrimitive]
        );
    }


    public void setStateChanged(bool _state) 
    {
        mStateChanged = _state;
    }
    public bool GetObjectState()
    {
        return mStateChanged;
    }

}


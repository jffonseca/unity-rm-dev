﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaymarchTorus : RaymarchObject
{
    private const string primitive = "TORUS";
    [Header(primitive)]
    [Range(0.0f, 2.0f)]
    public float _RadiusA = 1.0f;
    [Range(0.0f, 2.0f)]
    public float _RadiusB = 0.5f;
    private float oldRadiusA, oldRadiusB;


    string mSdf = "float4(_MaterialAlbedo[__index].rgb, sdTorus(mul(_ObjectTransformMatrix[__index], float4(p, 1)).xyz, float2(__RADIUS_A,__RADIUS_B)))";

    private void Update()
    {
        setStateChanged(false);
        if ( (_RadiusA != oldRadiusA) )
        {
            oldRadiusA = _RadiusA;
            setStateChanged(true);
        }
        else if ((_RadiusB != oldRadiusB))
        {
            oldRadiusB = _RadiusB;
            setStateChanged(true);
        }
    }
    public override string GetDistanceFunction()
    {
        string output = mSdf.Replace("__RADIUS_A", _RadiusA + "");
        output = output.Replace("__RADIUS_B", _RadiusB + "");

        return string.Format(
           output
        );
    }
}

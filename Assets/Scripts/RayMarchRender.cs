﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RayMarchRender : SceneViewFilter
{

    public ComputeShader _RayMarchCompute;
    public Shader _SceneDraw;

    [Header("Ray March")]
    public int _MaxIterations;
    [Range(0.01f, 1.0f)]
    public float _StepScale;
    public float _MaxDistance;
    [Range(0.1f, 0.0001f)]
    public float _Accuracy;
    public Texture2D _NoiseTex;

    [Header("Shadow")]
    [Range(0, 4)]
    public float _ShadowIntensity;
    public Vector2 _ShadowDistance;
    [Range(1, 128)]
    public float _ShadowPenumbra;

    [Header("Ambient Occlusion")]
    [Range(1, 5)]
    public int _AoIterations;
    [Range(0.01f, 10.0f)]
    public float _AoStepSize;
    [Range(0, 1)]
    public float _AoIntensity;

    [Header("Reflection")]
    [Range(0, 2)]
    public int _ReflectionCount;
    [Range(0, 1)]
    public float _ReflectionIntensity;
    [Range(0, 3)]
    public float _EnvRefIntensity;

    [Range(0, 4)]
    public float _ColorIntensity;

    private RenderTexture _target;
    private ComputeBuffer _sphereBuffer;
    private Camera _camera;
    private Light[] _lights;
    private RaymarchObject[] _objects;
    //private Texture2DArray _albedoTexture;
   // private Texture2DArray _normalTexture;

    private ReflectionProbe probeComponent = null;
    private Cubemap cubemap = null;
    private int renderId = -1;

    public Material _rayMarchMaterial
    {
        get
        {
            if (!_rayMarchMat && _SceneDraw)
            {
                _rayMarchMat = new Material(_SceneDraw);
                _rayMarchMat.hideFlags = HideFlags.HideAndDontSave;
            }
            return _rayMarchMat;
        }
    }
    private Material _rayMarchMat;

    private void Start()
    {
        //Application.targetFrameRate = 60;

        int textureSize = 1024;
        _objects = FindObjectsOfType(typeof(RaymarchObject)) as RaymarchObject[];
        /*
        // Create Texture2DArray
        _albedoTexture = new Texture2DArray(textureSize, textureSize, _objects.Length, TextureFormat.RGBA32, true, false);
        // Apply settings
        _albedoTexture.filterMode = FilterMode.Bilinear;
        _albedoTexture.wrapMode = TextureWrapMode.Repeat;

        // Create Texture2DArray
        _normalTexture = new Texture2DArray(textureSize, textureSize, _objects.Length, TextureFormat.RGBA32, true, false);
        // Apply settings
        _normalTexture.filterMode = FilterMode.Bilinear;
        _normalTexture.wrapMode = TextureWrapMode.Repeat;

        int i = 0;
        foreach (RaymarchObject objects in _objects)
        {     
            _albedoTexture.SetPixels(objects.GetAlbedoTexture().GetPixels(0), i, 0);
            _normalTexture.SetPixels(objects.GetNormalTexture().GetPixels(0), i, 0);
            i++;
        }
         _albedoTexture.Apply();
        _normalTexture.Apply();
*/
        createSkybox();
    }

    private void createSkybox() {
        GameObject probeGameObject = new GameObject("Default Reflection Probe");

        // Use a location such that the new Reflection Probe will not interfere with other Reflection Probes in the Scene.
        probeGameObject.transform.position = new Vector3(0, -1000, 0);

        // Create a Reflection Probe that only contains the Skybox. The Update function controls the Reflection Probe refresh.
        probeComponent = probeGameObject.AddComponent<ReflectionProbe>() as ReflectionProbe;
        probeComponent.resolution = 256;
        probeComponent.size = new Vector3(1, 1, 1);
        probeComponent.cullingMask = 0;
        probeComponent.clearFlags = UnityEngine.Rendering.ReflectionProbeClearFlags.Skybox;
        probeComponent.mode = UnityEngine.Rendering.ReflectionProbeMode.Realtime;
        probeComponent.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting;
        probeComponent.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.NoTimeSlicing;

        // A cubemap is used as a default specular reflection.
        cubemap = new Cubemap(probeComponent.resolution, probeComponent.hdr ? TextureFormat.RGBAHalf : TextureFormat.RGBA32, true);
    }

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }
    private void SetShaderParameters()
    {
        _RayMarchCompute.SetMatrix("_CameraToWorld", _camera.cameraToWorldMatrix);
        _RayMarchCompute.SetMatrix("_CameraInverseProjection", _camera.projectionMatrix.inverse);


        _RayMarchCompute.SetInt("_MaxIterations", _MaxIterations);
        _RayMarchCompute.SetFloat("_StepScale", _StepScale);
        _RayMarchCompute.SetFloat("_MaxDistance", _MaxDistance);
        _RayMarchCompute.SetFloat("_Accuracy", _Accuracy);
       // _RayMarchCompute.SetTexture("_NoiseTex", _NoiseTex);


        setShaderObjects();
        setShaderLights();

        _RayMarchCompute.SetFloat("_ShadowIntensity", _ShadowIntensity);
        _RayMarchCompute.SetVector("_ShadowDistance", _ShadowDistance);
        _RayMarchCompute.SetFloat("_ShadowPenumbra", _ShadowPenumbra);

        _RayMarchCompute.SetInt("_AoIterations", _AoIterations);
        _RayMarchCompute.SetFloat("_AoStepSize", _AoStepSize);
        _RayMarchCompute.SetFloat("_AoIntensity", _AoIntensity);

        _RayMarchCompute.SetInt("_ReflectionCount", _ReflectionCount);
        _RayMarchCompute.SetFloat("_ReflectionIntensity", _ReflectionIntensity);
        _RayMarchCompute.SetFloat("_EnvRefIntensity", _EnvRefIntensity);
        _RayMarchCompute.SetTexture(0,"_ReflectionCube", cubemap);


        _RayMarchCompute.SetFloat("_ColorIntensity", _ColorIntensity);
    }

    private void Render(RenderTexture destination)
    {
        // Make sure we have a current render target
        InitRenderTexture();
        // Set the target and dispatch the compute shader
        _RayMarchCompute.SetTexture(0, "Result", _target);
        int threadGroupsX = Mathf.CeilToInt(Screen.width / 32.0f);
        int threadGroupsY = Mathf.CeilToInt(Screen.height / 32.0f);
        _RayMarchCompute.Dispatch(0, threadGroupsX, threadGroupsY, 1);
        // Blit the result texture to the screen
        Graphics.Blit(_target, destination);
    }

    private void InitRenderTexture()
    {
        if (_target == null || _target.width != Screen.width || _target.height != Screen.height)
        {
            // Release render texture if we already have one
            if (_target != null)
                _target.Release();
            // Get a render target for Ray Tracing
            _target = new RenderTexture(Screen.width, Screen.height, 0,
                RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            _target.enableRandomWrite = true;
            _target.Create();
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {

        if (!_rayMarchMaterial)
        {
            Graphics.Blit(source, destination);
            return;
        }
        _RayMarchCompute.SetTexture(0, "_WorldTex", source);
        SetShaderParameters();
        Render(destination);

        RenderTexture.active = destination;
        
        _rayMarchMaterial.SetTexture("_MainTex", _target);

        GL.PushMatrix();
        GL.LoadOrtho();
        _rayMarchMaterial.SetPass(0);
        GL.Begin(GL.QUADS);

        //BL
        GL.MultiTexCoord2(0, 0.0f, 0.0f);
        GL.Vertex3(0.0f, 0.0f, 0.0f);
        //BR
        GL.MultiTexCoord2(0, 1.0f, 0.0f);
        GL.Vertex3(1.0f, 0.0f, 0.0f);
        //TR
        GL.MultiTexCoord2(0, 1.0f, 1.0f);
        GL.Vertex3(1.0f, 1.0f, 0.0f);
        //TL
        GL.MultiTexCoord2(0, 0.0f, 1.0f);
        GL.Vertex3(0.0f, 1.0f, 0.0f);

        GL.End();
        GL.PopMatrix();

        // The texture associated with the real-time Reflection Probe is a render target and RenderSettings.customReflection is a Cubemap. We have to check the support if copying from render targets to Textures is supported.
        if ((SystemInfo.copyTextureSupport & UnityEngine.Rendering.CopyTextureSupport.RTToTexture) != 0)
        {
            // Wait until previous RenderProbe is finished before we refresh the Reflection Probe again.
            // renderId is a token used to figure out when the refresh of a Reflection Probe is finished. The refresh of a Reflection Probe can take mutiple frames when time-slicing is used.
            if (renderId == -1 || probeComponent.IsFinishedRendering(renderId))
            {
                if (probeComponent.IsFinishedRendering(renderId))
                {
                    // After the previous RenderProbe is finished, we copy the probe's texture to the cubemap and set it as a custom reflection in RenderSettings.
                    Graphics.CopyTexture(probeComponent.texture, cubemap as Texture);

                    RenderSettings.customReflection = cubemap;
                    RenderSettings.defaultReflectionMode = UnityEngine.Rendering.DefaultReflectionMode.Custom;
                }

                renderId = probeComponent.RenderProbe();
            }
        }
    


}


    private void setShaderObjects()
    {
        _objects = FindObjectsOfType(typeof(RaymarchObject)) as RaymarchObject[];
        Vector4[] albedo = new Vector4[_objects.Length];
        Vector4[] parameters = new Vector4[_objects.Length];
        Matrix4x4[] matrix = new Matrix4x4[_objects.Length];
        Vector4[] objectType = new Vector4[_objects.Length];
        int numberOfObjects = 0;

        
        foreach (RaymarchObject objects in _objects)
        {
            albedo[numberOfObjects] = (objects.GetAlbedo());
            parameters[numberOfObjects] = new Vector4(objects.GetSpecular(), objects.GetReflectivity(), 0,0);
            matrix[numberOfObjects] = (objects.GetTransform());
            objectType[numberOfObjects] = new Vector4(objects.GetObjectType(),0,0,0);

            numberOfObjects++;
        }
        _RayMarchCompute.SetVectorArray(Shader.PropertyToID("_MaterialAlbedo"), albedo);
      //  _RayMarchCompute.SetTexture(0, Shader.PropertyToID("_MaterialAlbedoTexture"), _albedoTexture);
       // _RayMarchCompute.SetTexture(0, Shader.PropertyToID("_MaterialNormalTexture"), _normalTexture);
        _RayMarchCompute.SetVectorArray(Shader.PropertyToID("_MaterialParameters"), parameters);
        _RayMarchCompute.SetMatrixArray(Shader.PropertyToID("_ObjectTransformMatrix"), matrix);
        _RayMarchCompute.SetInt("_NumberOfObjects", numberOfObjects);
        _RayMarchCompute.SetVectorArray(Shader.PropertyToID("_ObjectType"), objectType);
    }

    private void setShaderLights()
    {

        int numberOfLights = 0;
        _lights = FindObjectsOfType(typeof(Light)) as Light[];
        Vector4[] ld = new Vector4[_lights.Length];
        Vector4[] lc = new Vector4[_lights.Length];
        float[] li = new float[_lights.Length];

        foreach (Light light in _lights)
        {
            ld[numberOfLights] = new Vector4(light.transform.forward.x, light.transform.forward.y, light.transform.forward.z, light.intensity);
            lc[numberOfLights] = (light.color);
            li[numberOfLights] = (light.intensity);
            numberOfLights++;
        }

        _RayMarchCompute.SetVectorArray(Shader.PropertyToID("_LightDir"), ld);
        _RayMarchCompute.SetVectorArray(Shader.PropertyToID("_LightColor"), lc);
        _RayMarchCompute.SetInt("_NumberOfLights", numberOfLights);
    }
}
